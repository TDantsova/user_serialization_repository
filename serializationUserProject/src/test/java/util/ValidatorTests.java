package util;

import org.junit.Assert;
import org.junit.Test;

import static util.Validator.*;

public class ValidatorTests {
    @Test
    public void fieldNameMustBeAlpha() {
        Assert.assertTrue(isAlphabet("Andrey"));
    }

    @Test
    public void fieldNameMustNotBeJustSmallAlphabet() {
        Assert.assertFalse(isAlphabet("boris"));
    }

    @Test
    public void fieldNameMustNotBeEmpty() {
        Assert.assertFalse(isAlphabet(""));
    }

    @Test
    public void fieldNameMustNotContainSpecialCharsAndNumbers() {
        Assert.assertFalse(isAlphabet("!123$"));
    }

    @Test
    public void fieldPhoneNumberMustBeSpecialFormat() {
        Assert.assertTrue(isValidatePhoneNumber("37500 1234567"));
    }

    @Test
    public void fieldPhoneNumberMustNotBe() {
        Assert.assertFalse(isValidatePhoneNumber("7350123"));
    }

    @Test
    public void fieldEmailMustBeSpecialFormat() {
        Assert.assertTrue(isValidateEmail("any@email.com"));
    }

    @Test
    public void fieldEmailMustContainTwoSpecialCharacters() {
        Assert.assertFalse(isValidateEmail("any.com"));
    }

    @Test
    public void fieldRolesMustAlphabetAndNumbersAnd_() {
        Assert.assertTrue(isValidateRoles("QA"));
    }
}