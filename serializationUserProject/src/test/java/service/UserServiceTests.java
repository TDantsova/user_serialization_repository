package service;

import model.User;
import org.junit.*;

import static java.util.Objects.isNull;
import static org.junit.Assert.*;
import org.mockito.*;
import util.Сonstant;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class UserServiceTests {
    private static final String testFileName = ".\\src\\main\\resources\\userSerialization_test.dat";
    private static MockedStatic<Сonstant> env;
    private static UserService userService;
    private static User user;

    @BeforeClass
    public static void setUp() throws IOException {
        env = Mockito.mockStatic(Сonstant.class);
        env.when(Сonstant::getFileName).thenReturn(testFileName);
        user = new User(100, "Bob", "Jat", "a2s@o_t.mas", "cook", "37510 1234567");
        userService = new UserService();
    }

    @Before
    public void cleanUp() throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(testFileName));
        fos.close();
    }

    @Test
    public void addUserToFile_numberRecordsIncreases() throws IOException {
        userService.addUser(user);
        List<User> actualResultUserList = userService.getAllUsers();
        assertTrue("list of users must be equal 1", actualResultUserList.size() == 1);
    }

    @Test
    public void addDuplicateUser_shouldThrowRuntimeException() throws IOException {
        userService.addUser(user);
        try {
            userService.addUser(user);
        } catch (RuntimeException exception) {
            Assert.assertTrue(exception.getMessage().contains("already exists"));
        }
    }

    @Test
    public void getAllUsersFromFile_shouldReturnListUsers() throws IOException {
        User user2 = new User(2, "Tob", "Hat", "tob@o_t.mas", "cook", "37510 1234568");
        userService.addUser(user);
        userService.addUser(user2);
        Assert.assertTrue(userService.getAllUsers().size() == 2);
    }

    @Test
    public void getUserById_shouldReturnUser() throws IOException {
        userService.addUser(user);
        User expectedUser = userService.getUserById(user.getId());
        Assert.assertTrue(expectedUser.equals(user));
    }

    @Test
    public void getUserByNonExistId_shouldThrowException() throws IOException {
        userService.addUser(user);
        Integer nonExistUserId = 1000;
        try {
            Assert.assertTrue(isNull(userService.getUserById(nonExistUserId)));
        } catch (RuntimeException exception) {
            Assert.assertTrue(exception.getMessage().contains("not found"));
        }
    }

    @Test
    public void removeUserFromFile_shouldDecreaseRecords() throws IOException {
        userService.addUser(user);
        userService.removeUser(user.getId());
        List<User> users = userService.getAllUsers();
        Assert.assertTrue(users.isEmpty());
    }
}