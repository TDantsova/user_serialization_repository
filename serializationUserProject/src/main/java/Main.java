/**
 * App to create / edit / view users and save changes to a file.
 *
 * @author TDantsova
 */

public class Main {
    public static void main(String[] args) {
        ConsoleOperation consoleOperation = new ConsoleOperation();
        consoleOperation.showMenuItems();
        consoleOperation.selectMenuItem();
    }
}