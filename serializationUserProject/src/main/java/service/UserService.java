package service;

import model.User;
import repository.UserRepository;
import repository.FileUserRepository;

import java.io.IOException;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class UserService {
    private UserRepository userRepository;

    public UserService() throws IOException {
        this.userRepository = new FileUserRepository();
    }

    public void addUser(User userData) throws IOException {
        if (!hasUserToFile(userData.getId())) {
            userRepository.addUser(userData);
        } else throw new RuntimeException(String.format("User %s already exists", userData.toString()));
    }

    public User getUserById(Integer userId) throws IOException {
        User user = userRepository.getUserById(userId);
        if (isNull(user)) {
            throw new RuntimeException(String.format("User %s not found", userId));
        }
        return user;
    }

    public void updateUser(User userToUpdate) throws IOException {
            userRepository.updateUser(userToUpdate);
    }

    public void removeUser(Integer userId) throws IOException {
        userRepository.removeUserById(userId);
    }

    public List<User> getAllUsers() throws IOException {
        List<User> userList = userRepository.getAllUsers();
        return userList;
    }

    private boolean hasUserToFile(Integer id) throws IOException {
        return nonNull(userRepository.getUserById(id));
    }
}