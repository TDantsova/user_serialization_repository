import model.User;
import service.UserService;
import util.Validator;
import util.Сonstant;

import java.io.IOException;
import java.util.Scanner;

import static util.Validator.*;

public class ConsoleOperation {
    static Scanner scanner = new Scanner(System.in);
    private UserService userService;

    public ConsoleOperation() {
        try {
            this.userService = new UserService();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMenuItems() {
        System.out.println("___________________________");
        System.out.println("Menu:");
        System.out.println("1. Add new user.");
        System.out.println("2. Get user by id.");
        System.out.println("3. Update user by id.");
        System.out.println("4. Remove user by id.");
        System.out.println("5. Get all user data.");
        System.out.println("6. EXIT.");
    }

    public void selectMenuItem() {
        int number;
        User user;

        while (true) {
            try {
                System.out.println("____________________________");
                System.out.print("Enter the menu item: ");
                number = Integer.parseInt(scanner.nextLine());
                switch (number) {
                    case 1:
                        user = getUserData();
                        userService.addUser(user);
                        System.out.println("User has been added to file, \n" +user.toString());
                        break;
                    case 2:
                        user = userService.getUserById(getEnteredUserId());
                        System.out.println(user.toString());
                        break;
                    case 3:
                        user = userService.getUserById(getEnteredUserId());
                        System.out.println(user.toString());
                        showMenuItemsEditedUser();
                        userService.updateUser(userSelectEditedMenuItem(user));
                        System.out.println("User has been update.");
                        break;
                    case 4:
                        userService.removeUser(getEnteredUserId());
                        System.out.println("This Entry has been deleted.");
                        break;
                    case 5:
                        System.out.println(userService.getAllUsers().toString().replaceAll("[), ]{3}", ")\n"));
                        break;
                    case 6:
                        System.exit(0);
                    default:
                        System.out.println("Incorrect input menu item, try again.");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Wrong number entered");
            } catch (IOException ex) {
                System.out.println("File storage not found.");
            } catch (RuntimeException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void showMenuItemsEditedUser() {
        System.out.println("What user param do you want edit?");
        System.out.println("1. first name");
        System.out.println("2. last name");
        System.out.println("3. email");
        System.out.println("4. roles");
        System.out.println("5. phone numbers");
    }

    public User userSelectEditedMenuItem(User editedUser) {
        int menuItem;
        while (true) {
            try {
                System.out.print("Enter the menu item: ");
                menuItem = Integer.parseInt(scanner.nextLine());
                switch (menuItem) {
                    case 1:
                        editedUser.setFirstName(getEnteredFirstName());
                        return editedUser;
                    case 2:
                        editedUser.setLastName(getEnteredLastName());
                        return editedUser;
                    case 3:
                        editedUser.setEmail(getEnteredEmail());
                        return editedUser;
                    case 4:
                        editedUser.setRoles(getEnteredRoles());
                        return editedUser;
                    case 5:
                        editedUser.setMobiles(getEnteredMobiles());
                        return editedUser;
                }
            } catch (NumberFormatException ex) {
                System.out.println("Wrong number entered");
            }
        }
    }

    private User getUserData() {
        System.out.println("Input data of user to update: ");
        return new User.Builder()
                .setFirstName(getEnteredFirstName())
                .setLastName(getEnteredLastName())
                .setEmail(getEnteredEmail())
                .setRole(getEnteredRoles())
                .setMobile(getEnteredMobiles())
                .build();
    }

    private Integer getEnteredUserId() {
        Integer id;
        while (true) {
            System.out.print("Input user's id: ");
            try {
                id = Integer.parseInt(scanner.nextLine());
                break;
            } catch (NumberFormatException ex) {
                System.out.println("Incorrect id, try again");
            }
        }
        return id;
    }

    private String getEnteredFirstName() {
        String firstName;
        while (true) {
            System.out.print("firstName = ");
            firstName = scanner.nextLine().trim();
            if (Validator.isAlphabet(firstName)) {
                break;
            } else System.out.println("Incorrect firstName, try again");
        }
        return firstName;
    }

    private String getEnteredLastName() {
        String lastName;
        while (true) {
            System.out.print("lastName = ");
            lastName = scanner.nextLine().trim();
            if (Validator.isAlphabet(lastName)) {
                break;
            } else System.out.println("Incorrect lastName, try again");
        }
        return lastName;
    }

    private String getEnteredEmail() {
        String email;
        while (true) {
            System.out.print("email(*@*.*) = ");
            email = scanner.nextLine().trim();
            if (isValidateEmail(email)) {
                break;
            } else
                System.out.println("Incorrect email, try again");
        }
        return email;
    }

    private String getEnteredRoles() {
        StringBuilder roles;
        while (true) {
            roles = new StringBuilder("");
            System.out.print("roles (no more than 3), for example, admin, dev_PHP) = ");
            String[] massRoles = scanner.nextLine().split(",");
            int count = 0;
            if (massRoles.length > Сonstant.getCountRolesUser()) {
                System.out.println("Incorrect count roles, no more than " + Сonstant.getCountRolesUser());
            } else {
                for (String role : massRoles) {
                    if (isValidateRoles(role.trim())) {
                        roles.append(role + "; ");
                        count++;
                    } else {
                        System.out.println("Incorrect format role " + role + " , try again");
                        roles.setLength(0);
                    }
                }
            }
            if (roles.length() != 0)
                break;
        }
        return roles.toString().trim();
    }

    private String getEnteredMobiles() {
        StringBuilder mobiles;
        while (true) {
            mobiles = new StringBuilder("");
            System.out.print("phone number (no more than 3), for example,375** *******,..) = ");
            String[] massMobiles = scanner.nextLine().split(",");
            int count = 0;
            if (massMobiles.length > Сonstant.getCountMobilesUser()) {
                System.out.println("Incorrect count phone numbers, no more than " + Сonstant.getCountMobilesUser());
            } else {
                for (String mobile : massMobiles) {
                    if (isValidatePhoneNumber(mobile.trim())) {
                        mobiles.append(mobile + "; ");
                        count++;
                    } else {
                        System.out.println("Incorrect phone number " + mobile + ", try again");
                        mobiles.setLength(0);
                    }
                }
            }
            if (mobiles.length() != 0) {
                break;
            }
        }
        return mobiles.toString().trim();
    }
}