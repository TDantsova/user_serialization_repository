package model;
import java.io.Serializable;

import java.util.Objects;

public class User implements Serializable {
    final long serialVersionUID = 1L;

    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String role;
    private String mobile;

    public User() {
    }

    public User(Integer id, String firstName, String lastName, String email, String role, String mobile) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.mobile = mobile;
    }

    public Integer getId() {return id; }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRoles(String role) {
        this.role = role;
    }

    public void setMobiles(String mobile) {
        this.mobile = mobile;
    }

    public static class Builder {
        private String firstName;
        private String lastName;
        private String email;
        private String role;
        private String mobile;

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setRole(String role) {
            this.role = role;
            return this;
        }

        public Builder setMobile(String mobile) {
            this.mobile = mobile;
            return this;
        }

        public User build() {
            return new User(hashCode(), firstName, lastName, email, role, mobile);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        User user = (User) obj;
        return  Objects.equals(lastName, user.lastName) &
                Objects.equals(email, user.email) &
                Objects.equals(mobile, user.mobile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, email, role, mobile);
    }

    @Override
    public String toString() {
        return "User (" + "id=" + id + ", name=" + firstName + ", lastName=" + lastName + ", email=" + email + ", role=" + role + ", mobile=" + mobile + ")";
    }
}