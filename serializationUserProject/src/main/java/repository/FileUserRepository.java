package repository;

import model.User;
import util.Сonstant;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUserRepository implements UserRepository {
    private final File file;

    public FileUserRepository() throws IOException {
        file = new File(Сonstant.getFileName());
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    @Override
    public void addUser(User user) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(new File(Сonstant.getFileName()), true);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(user);
        }
    }

    @Override
    public User getUserById(Integer id) throws IOException {
        User user;
        try (FileInputStream fis = new FileInputStream(Сonstant.getFileName())) {
            while (true) {
                ObjectInputStream ois = new ObjectInputStream(fis);
                user = (User) ois.readObject();
                if (user.getId().equals(id)) {
                    return user;
                }
            }
        } catch (EOFException | ClassNotFoundException e) {
            return null;
        }
    }

    @Override
    public void updateUser(User user) throws IOException {
        removeUserById(user.getId());
        addUser(user);
    }


    @Override
    public void removeUserById(Integer id) throws IOException {
        List<User> listFromFile = getAllUsers();
        List<User> editedList = new ArrayList<>();
        for (User user : listFromFile) {
            if (!user.getId().equals(id)) {
                editedList.add(user);
            }
        }
        try (FileOutputStream fos = new FileOutputStream(new File(Сonstant.getFileName()))) {
            for (User user : editedList) {
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(user);
            }
        }
    }

    @Override
    public List<User> getAllUsers() throws IOException {
        List<User> userList = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(new File(Сonstant.getFileName()))) {
            while (true) {
                ObjectInputStream ois = new ObjectInputStream(fis);
                userList.add((User) ois.readObject());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (EOFException e) {
            return userList;
        }
        return userList;
    }
}