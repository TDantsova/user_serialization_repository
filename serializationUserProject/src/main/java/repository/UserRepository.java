package repository;

import model.User;

import java.io.IOException;
import java.util.List;

public interface UserRepository {

    void addUser(User user) throws IOException;

    User getUserById(Integer id) throws IOException;

    void updateUser(User user) throws IOException;

    void removeUserById(Integer id) throws IOException;

    List<User> getAllUsers() throws IOException;
}