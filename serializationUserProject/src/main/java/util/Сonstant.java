package util;

public class Сonstant {
    private static final String FILE_NAME = ".\\src\\main\\resources\\userSerialization.dat";

    private static final Integer COUNT_ROLES_USER = 3;
    private static final Integer COUNT_MOBILES_USER = 3;

    public static String getFileName() {
        return FILE_NAME;
    }

    public static Integer getCountRolesUser() {
        return COUNT_ROLES_USER;
    }

    public static Integer getCountMobilesUser() {
        return COUNT_MOBILES_USER;
    }
}