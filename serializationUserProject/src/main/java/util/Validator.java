package util;

import java.util.regex.Pattern;

public class Validator {

    public static boolean isAlphabet(String name){
        Pattern pattern = Pattern.compile("^[A-Z][a-z]+");
        return pattern.matcher(name).matches() && name != null && !("".equals(name));
    }

    public static boolean isValidatePhoneNumber(String phoneNumber) {
        Pattern pattern = Pattern.compile("(375)[0-9]{2}\\s+[0-9]{7}");     //375** *******
        return pattern.matcher(phoneNumber).matches();
    }

    public static boolean isValidateEmail(String email) {
        Pattern pattern = Pattern.compile("\\w+[@]\\w+[.]\\w+");             //*@*.*
        return pattern.matcher(email).matches();
    }

    public static boolean isValidateRoles(String role){
        Pattern pattern = Pattern.compile("^(\\w+)");
        return pattern.matcher(role).matches();
    }
}